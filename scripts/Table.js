'use strict';

function BaseTable (matrix) {
	const self = this;

	this.isResolved = false;
	this.deletedRowColumnClass = 'deleted-row-column';
	this.container = document.getElementById(self.containerID);

	this.nextTable;
	this.table;
	this.tbody;
	this.colgroup;
	this.containerID;

	this.deleteRow = function (rowIndex) {
		if (self.tbody.childNodes.length < 2) {
			return;
		}

		removeHighlightOfDeletedElement();
		highlightDeletedElement(self.tbody.rows[rowIndex]);

		let nextTableMatrix = self.deepCopy(matrix);
		nextTableMatrix.data.splice(rowIndex, 1);

		nextTableMatrix.player2Strategies.splice(rowIndex, 1);

		return nextTableMatrix;
	};
	this.deleteColumn = function (columnIndex) {
		if (self.colgroup.childNodes.length < 3){
			return;
		}

		removeHighlightOfDeletedElement();
		highlightDeletedElement(self.colgroup.childNodes[columnIndex + 1]);

		let nextTableMatrix = self.deepCopy(matrix);
		nextTableMatrix.data.forEach(function (row) {
			row.splice(columnIndex, 1);
		});
		nextTableMatrix.player1Strategies.splice(columnIndex, 1);

		return nextTableMatrix;
	};
	this.remove = function () {
		if(self.nextTable) {
			self.nextTable.remove();
		}

		self.table.remove();
		self.removeCustomElements()
	};
	this.removeCustomElements = function () {};
	this.addCustomListeners = function () {};
	this.addTable = function () {
		injectTable();
		self.addAdditionalTableElements();
		self.addCustomListeners();
	};
	this.addAdditionalTableElements = function () {};
	this.deepCopy = function (matrix) {
		return JSON.parse(JSON.stringify(matrix));
	};

	function updateTable () {
		self.remove();
		self.addTable();
	}

	function scrollToBottomOfPage() {
		window.scrollTo(0, document.body.scrollHeight);
	}

	function injectTable () {
		self.container.appendChild(createTable());
		addInputChangesListener();
		scrollToBottomOfPage();
	}

	function addInputChangesListener () {
		self.table.addEventListener('input', function (event) {
			if (event.target.tagName.toLowerCase() === 'input') {
				removeHighlightOfDeletedElement();
				if (self.nextTable) {
					self.nextTable.remove();
				}

				let newValue = Number.parseInt(event.target.value) || 0;

				let cellIndex = event.target.parentNode.cellIndex - 1;
				let rowIndex = event.target.parentNode.parentNode.rowIndex - 1;
				let inputIndex = event.target.getAttribute('player') - 1;

				matrix.data[rowIndex][cellIndex][inputIndex] = newValue;

				updateTable();
			}
		});
	}

	function createColgroup (tableSize) {
		let colgroup = document.createElement('colgroup');

		for(let counter = 0; counter <= tableSize; counter++){
			colgroup.appendChild(document.createElement('col'));
		}

		return colgroup;
	}

	function removeHighlightOfDeletedElement () {
		if(self.highlightDeletedElement) {
			self.highlightDeletedElement.classList.remove(self.deletedRowColumnClass);
		}
	}

	function createDataRow(data, letter) {
		let row = document.createElement('tr');

		let th = document.createElement('th');
		th.innerText = letter + '1';

		row.appendChild(th);

		data.forEach(function (data) {
			let td = document.createElement('td');

			let inputFirstNumber = document.createElement('input');
			inputFirstNumber.setAttribute('player', '1');
			inputFirstNumber.value = data[0];

			let inputSecondNumber = document.createElement('input');
			inputSecondNumber.setAttribute('player', '2');
			inputSecondNumber.value = data[1];

			td.appendChild(inputFirstNumber);
			td.appendChild(inputSecondNumber);

			row.appendChild(td);
		});

		return row;
	}

	function createHeadRow(letters) {
		let row = document.createElement('tr');

		row.appendChild(document.createElement('th'));

		for(let counter = 0; counter < letters.length; counter++){
			let cell = document.createElement('th');
			cell.innerText = letters[counter] + '2';

			row.appendChild(cell);
		}

		return row;
	}

	function highlightDeletedElement (element) {
		element.classList.add(self.deletedRowColumnClass);
		self.highlightDeletedElement = element;
	}

	function createTable (){
		self.table = document.createElement('table');

		self.colgroup = createColgroup(matrix.data[0].length);
		self.table.appendChild(self.colgroup);

		let thead = document.createElement('thead');
		thead.appendChild(createHeadRow(matrix.player1Strategies));
		self.table.appendChild(thead);

		self.tbody = document.createElement('tbody');
		matrix.data.forEach(function(rowData, index){
			self.tbody.appendChild(createDataRow(rowData, matrix.player2Strategies[index]));
		});

		self.table.appendChild(self.tbody);

		scrollToBottomOfPage();

		return self.table;
	}
}
