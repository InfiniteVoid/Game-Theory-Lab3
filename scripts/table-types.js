'use strict';

function AutoResolvingTable (matrix, immediateResolving){
	let self = this;

	this.containerID = 'autoResolveTablesWrapper';
	const betterStrategyClass = 'better-strategy';

	BaseTable.call(this, matrix);

	this.addCustomListeners = function () {
		self.btnAutoResolve.addEventListener('click', function () {
			resolve();
			self.isResolved = true;
		});

		self.table.addEventListener('input', function (event) {
			if (event.target.tagName.toLowerCase() === 'input') {
				showResolveButton()
			}
		});
	};

	this.addAdditionalTableElements = function () {
		self.btnAutoResolve = document.createElement('button');
		self.btnAutoResolve.innerHTML = 'Вирішити';
		self.btnAutoResolve.classList.add('hidden');
		self.container.appendChild(self.btnAutoResolve);
	};

	this.removeCustomElements = function(){
		self.btnAutoResolve.remove();
	};

	function resolve () {
		hideResolveButton();
		let answer = resolveMatrix();
		if (answer) {
			let nextTableMatrix = deleteWorseStrategy(answer);
			highlightBetterStrategy(answer.axis, answer.better);

			if (nextTableMatrix) {
				if (self.nextTable) {
					self.nextTable.remove();
				}

				self.nextTable = new AutoResolvingTable(nextTableMatrix, true);
			}
		}
	}

	function resolveMatrix() {
		let answer;

		let comparedRows = goThroughAllPairs(matrix.data.length, rowsComparator);

		if(comparedRows !== -1) {
			answer = {
				axis: 'row',
				worse: comparedRows.worse,
				better: comparedRows.better,
			}
		}

		if(!answer) {
			let comparedColumns = goThroughAllPairs(matrix.data[0].length, columnsComparator);
			if(comparedColumns !== -1) {
				answer = {
					axis: 'column',
					worse: comparedColumns.worse,
					better: comparedColumns.better,
				}
			}
		}

		return answer;
	}

	function rowsComparator (firstIndex, secondIndex) {
		let i = matrix.data[secondIndex];
		return compareStrategies(matrix.data[firstIndex], i, 0);
	}

	function columnsComparator (firstIndex, secondIndex) {
		let firstColumn = getColumn(firstIndex);
		let secondColumn = getColumn(secondIndex);

		return compareStrategies(firstColumn, secondColumn, 1);
	}

	function goThroughAllPairs (size, comparator) {
		for(let firstIndex = 0; firstIndex < size; firstIndex++){
			for(let secondIndex = 0; secondIndex < size; secondIndex++){
				if(firstIndex !== secondIndex) {
					if (comparator(firstIndex, secondIndex)) {
						return {
							better: firstIndex,
							worse: secondIndex
						}
					}
				}
			}
		}

		return -1;
	}

	function getColumn(index) {
		let column = [];

		matrix.data.forEach(function (row) {
			column.push(self.deepCopy(row[index]));
		});

		return column;
	}

	function compareStrategies(array1, array2, valueIndex) {
		for(let counter = 0; counter < array1.length; counter++) {
			if( array1[counter][valueIndex] <= array2[counter][valueIndex]){
				return false;
			}
		}

		console.log(array1);
		console.log(array2);

		return true;
	}

	function deleteWorseStrategy(answer) {
		let nextTableMatrix;

		if(answer.axis === 'row'){
			nextTableMatrix = self.deleteRow(answer.worse);
		} else {
			nextTableMatrix = self.deleteColumn(answer.worse);
		}

		return nextTableMatrix;
	}

	function hideResolveButton() {
		self.btnAutoResolve.classList.add('hidden');
	}

	function showResolveButton() {
		self.btnAutoResolve.classList.remove('hidden');
	}

	function highlightBetterStrategy (axis, index) {
		if(axis === 'row') {
			self.tbody.rows[index].classList.add(betterStrategyClass);
		} else {
			self.colgroup.childNodes[index + 1].classList.add(betterStrategyClass);
		}
	}

	this.addTable();

	if(immediateResolving) {
		resolve();
	} else {
		showResolveButton();
	}
}

function ManualResolvingTable (matrix) {
	let self = this;

	this.containerID = 'manualResolveTablesWrapper';
	let selectedRowClass = 'selected-row';
	let selectedColumnClass = 'selected-column';
	let rowSelectorClass = 'row-selector';
	let columnSelectorClass = 'column-selector';

	BaseTable.call(this, matrix);

	this.addAdditionalTableElements = function () {
		addColumnsSelectors();
		addRowsSelectors();
	};

	this.addCustomListeners = function () {
		self.table.addEventListener('click', function (event) {
			let nextTableMatrix;

			if (event.target.classList.contains(rowSelectorClass)) {
				nextTableMatrix = this.deleteRow(event.target.parentNode.rowIndex - 1);
			}
			if (event.target.classList.contains(columnSelectorClass)) {
				nextTableMatrix = this.deleteColumn(event.target.cellIndex - 1);
			}

			if(nextTableMatrix) {
				if(this.isResolved){
					this.nextTable.remove();
				}

				this.nextTable = new ManualResolvingTable(nextTableMatrix);
				this.isResolved = true;
			}
		}.bind(this));

		self.table.addEventListener('mouseover', function (event) {
			if(event.target.classList.contains(columnSelectorClass)){
				self.colgroup.childNodes[event.target.cellIndex].classList.add(selectedColumnClass);
			}

			if(event.target.classList.contains(rowSelectorClass)) {
				event.target.parentElement.classList.add(selectedRowClass);
			}
		}.bind(self));

		self.table.addEventListener('mouseout', function (event) {
			if(event.target.classList.contains(columnSelectorClass)){
				self.colgroup.childNodes[event.target.cellIndex].classList.remove(selectedColumnClass);
			}

			if(event.target.classList.contains(rowSelectorClass)) {
				event.target.parentElement.classList.remove(selectedRowClass);
			}
		}.bind(self));
	};

	function addColumnsSelectors () {
		let tfoot = document.createElement('tfoot');
		let selectorsRow = document.createElement('tr');

		for(let cellIndex = 0; cellIndex <= matrix.data[0].length; cellIndex++) {
			let cell = document.createElement('td');
			let arrow = document.createElement('div');
			arrow.classList.add('arrow', 'top');

			if(cellIndex) {
				cell.classList.add(columnSelectorClass);
				cell.appendChild(arrow);
			}

			selectorsRow.appendChild(cell);
		}

		tfoot.appendChild(selectorsRow);

		self.table.appendChild(tfoot);
	}

	function addRowsSelectors () {
		let rows = self.tbody.childNodes;

		rows.forEach(function (row) {
			let selectorCell = document.createElement('td');
			let arrow = document.createElement('div');
			selectorCell.classList.add(rowSelectorClass);
			arrow.classList.add('arrow', 'left');

			selectorCell.appendChild(arrow);
			row.appendChild(selectorCell);
		});
	}

	this.addTable();
}