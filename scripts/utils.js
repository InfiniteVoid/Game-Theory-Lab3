'use strict';

function LettersList() {
	let currentLetter = 0;
	let firstLetterNumber = 97;

	this.getLetter = function() {
		return String.fromCharCode(firstLetterNumber + currentLetter++)
	}
}