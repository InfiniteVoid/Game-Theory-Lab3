'use strict';

const btnNewTable = document.getElementById('newTable');
const inputPlayer1Strategies = document.getElementById('player1');
const inputPlayer2Strategies = document.getElementById('player2');
const inputNumberOfPaids = document.getElementById('numberOfPaids');

let autoResolvedTable;
let manualResolvedTable;

btnNewTable.addEventListener('click', function () {
	if (autoResolvedTable) {
		autoResolvedTable.remove();
	}
	if (manualResolvedTable) {
		manualResolvedTable.remove();
	}

	let matrix = generateMatrix();

	autoResolvedTable = new AutoResolvingTable(matrix);
	manualResolvedTable = new ManualResolvingTable(matrix);
	document.getElementsByTagName('section')[0].classList.add('show')
});

function generateMatrix() {
	let player1NumOfStrategies = inputPlayer1Strategies.value;
	let player2NumOfStrategies = inputPlayer2Strategies.value;
	let numberOfPaids = inputNumberOfPaids.value;

	let data = [];

	for(let rowIndex = 0; rowIndex < player2NumOfStrategies; rowIndex++){
		data.push([]);

		for(let cellIndex = 0; cellIndex < player1NumOfStrategies; cellIndex++){
			data[rowIndex].push(generateRandomCellValues(numberOfPaids));
		}
	}

	return addStrategiesNames(data);
}

function addStrategiesNames (data) {
	let matrix = {};

	matrix.data = data;
	matrix.player1Strategies = [];
	matrix.player2Strategies = [];

	let lettersList = new LettersList();
	let numberOfLetters = data[0].length;

	for(let counter = 0; counter < numberOfLetters; counter++){
		matrix.player1Strategies.push(lettersList.getLetter());
	}

	lettersList = new LettersList();

	numberOfLetters = data.length;
	for(let counter = 0; counter < numberOfLetters; counter++){
		matrix.player2Strategies.push(lettersList.getLetter());
	}

	return matrix;
}

function generateRandomCellValues(limit) {
	return [generateRandomNubmer(limit), generateRandomNubmer(limit)]
}

function generateRandomNubmer(limit){
	return Math.round(Math.random() * (limit * 2 + 1) - 0.5) - limit;
}